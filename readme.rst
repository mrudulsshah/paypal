###########################
Paypal REST API Integration
###########################

Hi,

I have implemented the Paypal REST API using CodeIgniter Framework 3.0.0, Paypal REST API 1.*

**************************
Code Ability
**************************

Can make Payment using credit cards and through paypal 

*******************
Server Requirements
*******************

PHP version 5.4 or newer is recommended.
Curl
JSON
MySQL
paypal/rest-api-sdk-php : "1.*"

It should work on 5.2.4 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.

************
Installation
************
There are a few simple steps which need to follow in order to test or use this code.

1. Setup a folder with name paypal in your local server. 
2. Checkout the code or copy the downloaded code in newly created Folder.
3. Create a a database and Run the script available in install folder.
4. Configure the necessary database server details in application/config/database.php 
   
   [Change values for the fields below  only]
   	
   	'hostname' => 'localhost',
	'username' => 'root',
	'password' => '',
	'database' => 'paypal',

That's All.
Now just try to run the code from your localhost or server when you may have uploaded th code.
****
NOTE
****
if you have setup the directory name otherthen "paypal" then do not forget to change value of baseurl in 
application/config/config.php file.  Put the name of your directory in below line

$config['base_url'] = 'http://localhost/NAME_OF_YOUR_DIRECTORY/'; 


*******
License
*******

Please see the `license
agreement <https://github.com/bcit-ci/CodeIgniter/blob/develop/user_guide_src/source/license.rst>`_.

*********
Resources
*********

-  `User Guide <http://www.codeigniter.com/docs>`_
-  `Language File Translations <https://github.com/bcit-ci/codeigniter3-translations>`_
-  `Community Forums <http://forum.codeigniter.com/>`_
-  `Community Wiki <https://github.com/bcit-ci/CodeIgniter/wiki>`_
-  `Community IRC <http://www.codeigniter.com/irc>`_

Report security issues to our `Security Panel <mailto:security@codeigniter.com>`_, thank you.

***************
Acknowledgement
***************

The CodeIgniter team would like to thank EllisLab, all the
contributors to the CodeIgniter project and you, the CodeIgniter user.