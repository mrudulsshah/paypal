<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General {

	function parseApiError($errorJson) {
		$msg = '';
		
		$data = json_decode($errorJson, true);
		if(isset($data['name']) && isset($data['message'])) {
			$msg .= $data['name'] . " : " .  $data['message'] . "<br/>";
		}
		if(isset($data['details'])) {
			$msg .= "<ul>";
			foreach($data['details'] as $detail) {
				$msg .= "<li>" . $detail['field'] . " : " . $detail['issue'] . "</li>";	
			}
			$msg .= "</ul>";
		}
		if($msg == '') {
			$msg = $errorJson;
		}	
		return $msg;
	}
}