<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PayPal\Api\PaymentExecution;
use PayPal\Api\Amount;
use PayPal\Api\CreditCard;
use PayPal\Api\CreditCardToken;
use PayPal\Api\FundingInstrument;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

class Paypal {

	public function __construct(){
            return $this->getApiContext();
    }


	public function getApiContext() {


	    // Define the location of the sdk_config.ini file
	    if (!defined("PP_CONFIG_PATH")) {
	        define("PP_CONFIG_PATH", dirname(__DIR__));
	    }

		$apiContext = new ApiContext(new OAuthTokenCredential(
			'AT2t0TkqLE5WPL7kj1RmETcGmkFgjjhxilNAzrQ__xWnG7lvjjcpQOM8AfaeegWDBvdYvMcKFKjeTlza',
			'EIj-ds2Xt3JgLereJofIcmJ7RC-hNs5tw1Bn-a9Gw_8xxn0ZBBYl7UninaggoQKLOUAZOnhuDG3AmHaC'
		));

		
		// Alternatively pass in the configuration via a hashmap.
		// The hashmap can contain any key that is allowed in
		// sdk_config.ini	
		
		/*$apiContext->setConfig(array(
			'http.ConnectionTimeOut' => 30,
			'http.Retry' => 1,
			'mode' => 'sandbox',
			'log.LogEnabled' => true,
			'log.FileName' => '../PayPal.log',
			'log.LogLevel' => 'INFO'		
		));*/
		
		return $apiContext;
	}

	public function saveCard($params) {
	
			$card = new CreditCard();
			$card->setType($params['type']);
			$card->setNumber($params['number']);
			$card->setExpireMonth($params['expire_month']);
			$card->setExpireYear($params['expire_year']);
			$card->setCvv2($params['cvv2']);
			$card->create($this->getApiContext());
			
			return $card->getId();
		}
	public function getCreditCard($cardId) {
		return CreditCard::get($cardId, getApiContext());
	}
		
	public function makePaymentUsingCC($creditCardId, $total, $currency, $paymentDesc) {
				
		$ccToken = new CreditCardToken();
		$ccToken->setCreditCardId($creditCardId);
		
		$fi = new FundingInstrument();
		$fi->setCreditCardToken($ccToken);
		
		$payer = new Payer();
		$payer->setPaymentMethod("credit_card");
			$payer->setFundingInstruments(array($fi));
		
		// Specify the payment amount.
		$amount = new Amount();
		$amount->setCurrency($currency);
		$amount->setTotal($total);
		// ###Transaction
		$transaction = new Transaction();
		$transaction->setAmount($amount);
		$transaction->setDescription($paymentDesc);
		
		$payment = new Payment();
		$payment->setIntent("sale");
		$payment->setPayer($payer);
		$payment->setTransactions(array($transaction));
		$payment->create($this->getApiContext());
		return $payment;
	}
	
	public function makePaymentUsingPayPal($total, $currency, $paymentDesc, $returnUrl, $cancelUrl) {
		
		$payer = new Payer();
		$payer->setPaymentMethod("paypal");
		
		// Specify the payment amount.
		$amount = new Amount();
		$amount->setCurrency($currency);
		$amount->setTotal($total);
		
		// ###Transaction
	
		$transaction = new Transaction();
		$transaction->setAmount($amount);
		$transaction->setDescription($paymentDesc);
		
		$redirectUrls = new RedirectUrls();
		$redirectUrls->setReturnUrl($returnUrl);
		$redirectUrls->setCancelUrl($cancelUrl);
		
		$payment = new Payment();
		$payment->setRedirectUrls($redirectUrls);
		$payment->setIntent("sale");
		$payment->setPayer($payer);
		$payment->setTransactions(array($transaction));
		
		$payment->create($this->getApiContext());
		return $payment;
	}
	
	public function executePayment($paymentId, $payerId) {
		
		$payment = getPaymentDetails($paymentId);
		$paymentExecution = new PaymentExecution();
		$paymentExecution->setPayerId($payerId);
		$payment = $payment->execute($paymentExecution, $this->getApiContext());
		
		return $payment;
	}
	
	public function getPaymentDetails($paymentId) {
		$payment = Payment::get($paymentId, $this->getApiContext());
		return $payment;
	}
}