<div class='col-sm-6'>
	<?php echo form_open('auth/signin_user') ?>
        <h2> Log In </h2>
        <div class='row form-group'>
            <label for='email' class='control-label col-sm-2'> Email: </label>
            <div class='col-sm-8'>
                <input type='text' name='user[email]' class='form-control' required />
            </div>
        </div>
        <div class='row form-group'>
            <label for='password' class='control-label col-sm-2'> Password: </label>
            <div class='col-sm-8'>
                <input type='password' name='user[password]' class='form-control' required />
            </div>
        </div>
        <div class='row form-group'>
            <div class='col-sm-offset-2 col-sm-10'>
                <button class='btn btn-primary'> Log In </button>
            	<span style="float: right;">New User ?</span>
            </div>

        </div>
    <?php form_close(); ?>
</div>                