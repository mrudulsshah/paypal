<?php if(isset($errorMessage)) {?>
        <div class="alert fade in alert-error">
            <button class="close" data-dismiss="alert">&times;</button>
            <?php echo $errorMessage;?>
        </div>
<?php }?>

<div class='col-sm-6'>
    <?php echo form_open('auth/signup_user', array('class' => 'form-horizontal', 'id' => 'new_user', 'autocomplete'=>'off','novalidate'=>'novalidate','data-toggle'=>'validator')) ?>   

        <h2> Sign Up </h2>
        <div class='row form-group'>
            <label for='user_email' class='control-label col-sm-4'> Email: </label>
            <div class='col-sm-8'>
                <input class="form-control" id="user_email" name="user[email]" size="50" type="email" value="" placeholder="dummy@email.com" required>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class='row form-group'>
            <label for='password' class='control-label col-sm-4'> Password </label>
            <div class='col-sm-8'>
                <input class="form-control" id="user_password" name="user[password]" size="50" type="password" required />
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class='row form-group'>
            <label for='password' class='control-label col-sm-4'>Confirm Password </label>
            <div class='col-sm-8'>
                <input class="form-control" id="user_password_confirmation" name="user[password_confirmation]" size="50" type="password" data-match="#user_password" data-match-error="Whoops, these don't match" required />
                <div class="help-block with-errors"></div>
            </div>
        </div>

        <br>
        <legend>Add Credit Card (Optional)</legend>
        
        <p><i>*Note: Credit card information will be stored with PayPal.</i></p>
        
        <div class='row form-group'>
            <label for='user_credit_card_type' class='control-label col-sm-4'>CC Type </label>
            <div class='col-sm-8'>
                <select class="form-control dropdown" id="user_credit_card_type" name="user[credit_card][type]">
                    <option value=""></option>
                    <option value="visa" selected>visa</option>
                    <option value="mastercard">mastercard</option>
                    <option value="discover">discover</option>
                    <option value="amex">amex</option>
                </select>
            </div>
        </div>

        <div class='row form-group'>
            <label for='user_credit_card_number' class='control-label col-sm-4'> CC Number </label>
            <div class='col-sm-8'>
                <input class="form-control" id="user_credit_card_number" name="user[credit_card][number]" size="50" type="text" value=""/>
            </div>
        </div>

        <div class='row form-group'>
            <label for='user_credit_card_cvv2' class='control-label col-sm-4'> CVV </label>
            <div class='col-sm-8'>
                <input class="form-control" id="user_credit_card_cvv2" name="user[credit_card][cvv2]" size="50" type="text" value="012" />
            </div>
        </div>

       
        <div class='row form-group'>
            <label for='user_credit_card_expire_month' class='control-label col-sm-4'>Expire month </label>
            <div class='col-sm-8'>
                <select class="form-control dropdown" id="user_credit_card_expire_month" name="user[credit_card][expire_month]">
                    <option value=""></option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option selected value="11">11</option>
                    <option value="12">12</option>
                </select>
            </div>
        </div>

        <div class='row form-group'>
            <label for='user_credit_card_expire_year' class='control-label col-sm-4'>Expire year </label>
            <div class='col-sm-8'>
                <select class="form-control dropdown" id="user_credit_card_expire_year" name="user[credit_card][expire_year]">
                    <option value=""></option>
                    <option value="2015">2015</option>
                    <option value="2016">2016</option>
                    <option value="2017">2017</option>
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                    <option selected value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option></select>
                </select>
            </div>
        </div>
        
        <div class='row form-group'>
            <div class='col-sm-offset-4 col-sm-10'>
                <button class='btn btn-primary'> Sign Up </button>
            </div>
        </div>
    <?php form_close(); ?>
</div>