<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $title; ?></title>
        <link rel='stylesheet' href='<?php echo base_url(); ?>packages/bootstrap/dist/css/bootstrap.min.css' />
        <link rel='stylesheet' href='<?php echo base_url(); ?>packages/css/bootstrap.min.css' />
    </head>
    <body>
        <div class='container'>
            <div class='row'>
            	<?php echo $body; ?>    
            </div>
        </div>
        <script src='<?php echo base_url(); ?>packages/jquery/dist/jquery.min.js'></script>
        <script src='<?php echo base_url(); ?>packages/bootstrap/dist/js/bootstrap.min.js'></script>
    </body>
</html>