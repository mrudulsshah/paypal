<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $title; ?></title>
        <link rel='stylesheet' href='<?php echo base_url(); ?>packages/bootstrap/dist/css/bootstrap.min.css' />
        <link rel='stylesheet' href='<?php echo base_url(); ?>packages/css/bootstrap.min.css' />
    </head>
    <body>
        <div class='container'>
            <div class='page-header'>
                <h1> Welcome ! </h1>
            </div>
            <div class='row'>
                <div class='col-sm-12'>
                    <?php echo $body; ?>    
                </div>
            </div>
        </div>
        <script src='<?php echo base_url(); ?>packages/jquery/dist/jquery.min.js'></script>
        <script src='<?php echo base_url(); ?>packages/jquery-validation/dist/jquery.validate.min.js'></script>
        <script src='<?php echo base_url(); ?>packages/jquery-validation/dist/additional-methods.min.js'></script>
        <script src='<?php echo base_url(); ?>packages/bootstrap/dist/js/bootstrap.min.js'></script>
        <script src='<?php echo base_url(); ?>packages/bootstrap/dist/js/bootstrap.validator.min.js'></script>
        <script src='<?php echo base_url(); ?>packages/js/custom.js'></script>

    </body>
</html>
