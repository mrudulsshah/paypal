<?php
class Users extends CI_Model {

        public function __construct()
        {
                parent::__construct();
        }

        public function addUser($email, $password, $creditCardId=NULL) {
	
			
			$this->email    		= $this->db->escape_str($email); 
            $this->password  		= $this->db->escape_str($password);
            $this->creditcard_id    = $this->db->escape_str($creditCardId);

            //$this->db->query($query, array($this->input->post('new_name'));

            $result = $this->db->insert('users', $this);

			if(!$result) {
				$errMsg = "Error creating user: " . $this->db->_error_message();
				return $errMsg;
			}
			
			return $this->db->insert_id();
		}

		public function validateLogin($email, $password) {
			
			$result =FALSE;

			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('email', $email);
			$this->db->where('password', $password);
			$this->db->limit(1); 
			$query = $this->db->get();

			if ($query->num_rows() > 0){
				
				/*foreach ($query->result() as $row){
		                 $this->session->set_userdata('email'=>$row->email);
		                 $this->session->set_userdata('email'=>$row->user_id);
		                 $this->session->set_userdata('isLoggedIn'=>TRUE);
		                
		        }*/
		        $result =TRUE;
			}
			
			if(!$result) {
				//return $errMsg = "Error validating login: " .$this->db->_error_message();
				return FALSE;
			}
			
			return $result;
		}


}