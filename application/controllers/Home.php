<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data = array(
		    'title' => 'Paypal',
		);
		
		$this->template->load('default', 'welcome_message', $data);
		//$this->load->view('welcome_message');
	}
	public function main(){
		
		//echo "<pre>";
		//print_r($this->session);die;
		if($this->session->userdata['isLoggedIn']){

			$data = array(
			    'title' => 'Home - Paypal',
			    'userdata' => $this->session->userdata()
			);
			
			$this->template->load('default', 'home/main', $data);	
		}
		redirect('auth/signin');
		exit;
		

	}
}
