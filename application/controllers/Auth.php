<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	
	public function index()
	{
		if( $this->session->userdata('isLoggedIn') ) {
        	redirect('home/main');
	    } else {
	        $this->signin(false);
	    }
		
	}

	public function signin($show_error = false){

		if( $this->session->userdata('isLoggedIn') ) {
        	redirect('home/main');
        	exit();
	    }

		$data = array(
		    'title' => 'Login-Paypal',
		);
		$this->template->load('authentication', 'auth/signin', $data);

	}
	public function signin_user(){
		if($this->input->server('REQUEST_METHOD') == 'POST'){
			try {
				$this->load->model('Users');
				$password = $this->encryption->encrypt($_POST['user']['password']);
				if($this->Users->validateLogin($_POST['user']['email'], $password)){
					$session_data = array(['email'=>$_POST['user']['email'],'userId'=>$userId,'isLoggedIn'=>TRUE]);
					$this->session->set_userdata($session_data);
					print_r($this->session);die;
					redirect('home/main');
					exit;
				} else {
					$errorMessage = "Login failed. Please check your username/password";
				}		
			} catch (Exception $ex) {
				$errorMessage = $ex->getMessage();
			}

		}
	}
	public function signup($show_error = false){
		$data = array(
		    'title' => 'Signup-Paypal',
		);

		$this->template->load('authentication', 'auth/signup', $data);
	}
	public function signup_user(){
		if($this->input->server('REQUEST_METHOD') == 'POST'){
			
			$userData = $_POST['user'];
			$cardData = $_POST['user']['credit_card'];
			
			if(trim($_POST['user']['email']) == '' || $_POST['user']['password'] == '') {
				$errorMessage = "You must enter a email address and password to register.";
			} else if ($_POST['user']['password'] != $_POST['user']['password_confirmation']) {
				$errorMessage = "Passwords do not match. Please check.";
			} else {
				try {			
					
					$creditCardId = NULL;
		            
		            if (isset($_POST['user']['credit_card']['cvv2']) && trim($_POST['user']['credit_card']['cvv2']) == '') {
		                unset($_POST['user']['credit_card']['cvv2']);
		            }
					
					if(trim($_POST['user']['credit_card']['number']) != "") {
						$creditCardId = $this->paypal->saveCard($_POST['user']['credit_card']);
					}
					
					$this->load->model('Users');
					$userId = $this->Users->addUser($_POST['user']['email'], $this->encryption->encrypt($_POST['user']['password']), $creditCardId);			
					
				} catch(\PayPal\Exception\PPConnectionException $ex){
					$errorMessage = $ex->getData() != '' ? $this->general->parseApiError($ex->getData()) : $ex->getMessage();
				} catch (Exception $ex) {
					$errorMessage = $ex->getMessage();		
				}
			}

			if(isset($userId) && $userId != false) {
				
				$session_data = array(['email'=>$_POST['user']['email'],'userId'=>$userId,'isLoggedIn'=>TRUE]);
				$this->session->set_userdata($session_data);
				redirect('home/main');
				exit;
			}

		}else{
			redirect('auth/signup');
		}


	}
}
